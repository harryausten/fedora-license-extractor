#!/bin/sh -eu

output_dir=output
archive_base_url="https://archives.fedoraproject.org/pub/archive"
listfile=fullfilelist
release=25

BOLD="$(tput bold)"
RED="$BOLD$(tput setaf 1)"
GREEN="$BOLD$(tput setaf 2)"
RESET="$(tput sgr0)"

download_rpm()
{
    if all="$(grep "/$release/.*/$1-[0-9]" "$listfile")" &&
        one="$(echo "$all" | grep 'source')" ||
        one="$(echo "$all" | grep 'SRPMS')" ||
        one="$(echo "$all" | grep 'x86_64')"; then
        one="$(echo "$one" | head -1)"
    else
        echo "${RED}Package ($1) not found!${RESET}"
        return 1
    fi

    echo "Downloading $1 ($one)..."
    mkdir -p "$output_dir/$1"
    if ! curl -Ls "$archive_base_url/$one" > "$output_dir/$1/pkg.rpm"; then
        echo "${RED}Failed to download package ($1)!${RESET}"
        return 1
    fi
}

while read -r line; do
    if [ ! -f "$listfile" ]; then
        echo "Downloading list file..."
        wget "$archive_base_url/$listfile"
    fi
    package="$(echo "$line" | cut -d ':' -f 1)"
    if ! download_rpm "$package"; then
        printf ""
    fi
done < licenses.txt
